import { MetadataDescriptionData } from "./MetadataDescriptionData";
import { MetadataSeriesData } from "./MetadataSeriesData";

/**
* Contains metadata information about the published file as a whole.
*/
export interface MetadataData
{
    /**
     * The unique identifier for the metadata.
     *
     * This will be generated if missing.
     */
    uid: string;

    author: string | string[] | undefined;
    date: string | undefined;
    description: string | MetadataDescriptionData | undefined;
    editor: string | string[] | undefined;
    language: string | undefined;
    publisher: string | undefined;
    rights: string | undefined;
    series: MetadataSeriesData | undefined;
    subjects: string[] | undefined;
    title: string;
}
