/**
* Contains additional information for getting a description for a book.
*/
export interface MetadataDescriptionData
{
    /**
     * The content to put in the description.
     */
    contents: string | undefined;

    /**
     * The relative path to the publication file to use for the
     * description.
     */
    source: string | undefined;
}
