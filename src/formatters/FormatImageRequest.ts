/**
* Information about a specific image and the desired change.
*/
export class FormatImageRequest
{
    /**
    * The href to put into the image. This can be changed by the formatter as
    * needed. */
    href: string;

    /**
    * The absolute path to the image being processed.
    */
    imagePath: string;

    /**
    * The buffer that contains the loaded image.
    */
    buffer: Buffer | undefined;
    scale: number | undefined;
    grayscale: boolean | undefined;
    opaque: boolean | undefined;
    mime: any | undefined;
    extension: string | undefined;
}
