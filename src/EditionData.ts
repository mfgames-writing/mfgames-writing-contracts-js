import { EditionImagesData } from "./EditionImagesData";
import { MetadataData } from "./MetadataData";

/**
* Contains information about the different editions or variants of the
* publication. This is contained in the `editions` element of the main file,
* but TypeScript cannot define that.
*/
export interface EditionData extends MetadataData
{
    outputDirectory?: string;
    outputFilename?: string;
    extends?: string;
    format: string;
    theme: string;
    images?: EditionImagesData;
    rootDirectory?: string;
    publicationFile?: string;
}
