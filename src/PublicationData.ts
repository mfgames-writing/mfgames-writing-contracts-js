import { ContentData } from "./ContentData";
import { MetadataData } from "./MetadataData";

/**
* Interface that describes the contents of the publish file regardless of the
* file format.
*/
export interface PublicationData
{
    editions: any;
    metadata: MetadataData;
    contents: ContentData[];
    rootDirectory?: string;
    publicationFile?: string;
}
