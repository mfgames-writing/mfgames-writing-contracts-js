import { ExcludeContentData } from "./ExcludeContentData";
import { PipelineData } from "./PipelineData";

/**
* Describes a single content element within the publication.
*/
export interface ContentData
{
    element: string;
    number?: string;
    directory: string;
    source: string;

    /**
    * If true, indicates that this content is the first one for the "go to home"
    * navigation of ebooks.
    */
    start?: boolean;

    /**
    * If this is false, then the content is considered not part of the the linear
    * content of the book.
    */
    linear?: boolean;

    /**
    * If this is true, then the Liquid template should be applied against the
    * contents of the HTML file before it is rendered.
    */
    liquid?: boolean;

    /**
    * Contains the optional nested contents for this one. The nesting is
    * used to create structured data for TOC and the NCX files.
    */
    contents?: ContentData[];

    /**
    * Contains the parent content, if we are nested. Otherwise, it will be
    * undefined.
    */
    parent?: ContentData;

    /**
    * Contains process-specific elements.
    */
    process?: any;

    /**
    * Contains the optional exclusions for this content.
    */
    exclude?: ExcludeContentData;

    /**
    * Identifies the page number that this content should start at. If this is
    * part of a pattern, then only the first file will have the page number set.
    */
    page?: number;

    /**
    * Contains the additional pipeline processes to associate with this content.
    */
    pipeline?: PipelineData[];
}
