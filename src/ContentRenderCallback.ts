import { ContentArgs } from "./ContentArgs";

export interface ContentRenderCallback
{
    (content: ContentArgs): string;
}
