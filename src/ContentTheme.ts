/**
* The information about a specific content with rendering the theme.
*/
export interface ContentTheme
{
    /**
     * Gets the name of the stylesheet to use, this allows for segments of a
     * document to use a different stylesheet. If this is undefined, then
     * the default one should be used instead.
     */
    styleName: string | undefined;
}
