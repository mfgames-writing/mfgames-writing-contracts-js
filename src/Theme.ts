import { ContentArgs } from "./ContentArgs";
import { ContentLinkRenderCallback } from "./ContentLinkRenderCallback";
import { ContentRenderCallback } from "./ContentRenderCallback";
import { ContentStringRenderCallback } from "./ContentStringRenderCallback";
import { ContentTheme } from "./ContentTheme";
import { EditionArgs } from "./EditionArgs";

export interface Theme
{
    /** The name of the stylesheet as included in the generated HTML files. */
    stylesheetFileName: string;

    /**
    * An optional method to override rendering of blockquotes.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} text The contents of the element.
    * @return {string} The rendered HTML.
    */
    renderBlockquote?: ContentStringRenderCallback;

    /**
    * An optional method to override rendering of rules (breaks).
    *
    * @param {ContentArgs} content The contents to process.
    * @return {string} The rendered HTML.
    */
    renderRule?: ContentRenderCallback;

    /**
    * An optional method to override rendering of strong (bold).
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} text The contents of the element.
    * @return {string} The rendered HTML.
    */
    renderStrong?: ContentStringRenderCallback;

    /**
    * An optional method to override rendering of emphasis or italics.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} text The contents of the element.
    * @return {string} The rendered HTML.
    */
    renderEmphasis?: ContentStringRenderCallback;

    /**
    * An optional method to override rendering of an inline code.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} text The contents of the element.
    * @return {string} The rendered HTML.
    */
    renderCodeSpan?: ContentStringRenderCallback;

    /**
    * An optional method to override rendering of struck or deleted text.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} text The contents of the element.
    * @return {string} The rendered HTML.
    */
    renderDelete?: ContentStringRenderCallback;

    /**
    * An optional method to override rendering of a link.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} href The actual link.
    * @param {string} title The title of the element.
    * @param {string} text The text of the link.
    * @return {string} The rendered HTML.
    */
    renderLink?: ContentLinkRenderCallback;

    /**
    * An optional method to process the rendered image.
    *
    * @param {ContentArgs} content The contents to process.
    * @param {string} html The previously rendered HTML.
    * @return {string} The rendered HTML.
    */
    processImage?: ContentStringRenderCallback;

    /**
    * An optional method for post-processing rendered paragraphs.
    */
    processParagraph?: ContentStringRenderCallback;

    start(args: EditionArgs): Promise<EditionArgs>;
    finish(args: EditionArgs): Promise<EditionArgs>;
    renderHtml(args: ContentArgs): Promise<ContentArgs>;
    /**
    * Renders the HTML chrome and layout elements such as the html, head, and
    * body elements around the contents given by the contents.
    */
    renderLayout(args: ContentArgs): Promise<ContentArgs>;

    renderNavigationTitle(args: ContentArgs): string;

    /**
    * Renders the stylesheet with various substitions for the edition with an
    * optional mode to use for specifics.
    *
    * @param {EditionArgs} args The edition being processed.
    * @param {string|string[]} mode An optional mode list which will select a
    *   specific stylesheet if available.
    */
    renderStylesheet(args: EditionArgs, mode?: string | string[]): Buffer;

    renderTableOfContents(args: ContentArgs): Promise<ContentArgs>;

    /**
    * Retrieves information about a specific element, such as how it is will be
    * rendered with the resulting stylesheet.
    *
    * @param {ContentArgs} content The content currently being processed.
    */
    getContentTheme(content: ContentArgs): ContentTheme;
}
