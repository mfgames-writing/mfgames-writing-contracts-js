/**
* An interface to description various operations that should be applied to
* images for a given edition. This is used to scale images down or to ensure
* they are opaque or grayscale.
*/
export interface EditionImagesData
{
    /**
    * How much the source images should be scaled with 1.0 being no change.
    * Defaults to 1.0.
    */
    scale?: number;

    /**
    * Should the image be converted into grayscale? Defaults to false.
    */
    grayscale?: boolean;

    /** Indicates whether the image should be made opaque. Defaults to false.
    */
    opaque?: boolean;
}
