import { ContentArgs } from "./ContentArgs";

/**
* The common signature for optional methods that take a string and return it.
*/
export interface ContentStringRenderCallback
{
    (content: ContentArgs, text: string): string;
}
