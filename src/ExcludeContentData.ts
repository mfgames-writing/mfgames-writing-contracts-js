export interface ExcludeContentData
{
    /**
    * If provided, then the content will be excluded from the given editions
    * listed in the publication file.
    */
    editions?: string[];

    /**
    * If present and true, then the content is excluded from the table of
    * contents.
    */
    toc?: boolean;
}
