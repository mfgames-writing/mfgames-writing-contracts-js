import { ContentArgs } from "./ContentArgs";

export interface ContentLinkRenderCallback
{
    (content: ContentArgs, href: string, title: string, text: string): string;
}
