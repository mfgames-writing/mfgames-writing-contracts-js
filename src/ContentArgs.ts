import zpad = require("zpad");
import * as path from "path";
import { ContentData } from "./ContentData";
import { EditionArgs } from "./EditionArgs";
import { EditionData } from "./EditionData";
import { Formatter } from "./formatters";
import { PipelineData } from "./PipelineData";
import { Theme } from "./Theme";

/**
* Encapsulates the pipeline state for processing content for a given edition.
*/
export class ContentArgs
{
    public buffer: Buffer;
    public contentData: ContentData;
    public directory: string;
    public editionArgs: EditionArgs;
    public id: string;
    public metadata: any = {};
    public parent: ContentArgs;
    public pipeline?: PipelineData[];
    public process: any = {};
    public source: string;
    public text: string;

    constructor(args: EditionArgs, data: ContentData, sequence: number = 0)
    {
        // Save the rest of the arguments.
        this.id = `c${zpad(sequence, 4)}`;
        this.editionArgs = args;
        this.contentData = data;
        this.metadata = { ...data };
        this.pipeline = data.pipeline;

        // We can't process the paths if we don't have a source.
        if (data.source)
        {
            // Normalize the directory and filenames names.
            this.directory = path.dirname(data.source);

            if (data.directory)
            {
                this.directory = path.join(data.directory, this.directory);
            }

            if (!path.isAbsolute(this.directory))
            {
                this.directory = path.join(this.editionArgs.rootDirectory, this.directory);
            }

            this.source = path.basename(data.source);

            // Populate the metadata with information from the data.
            this.metadata["directory"] = this.directory;
        }
    }

    public get depth(): number
    {
        return this.parent ? this.parent.depth + 1 : 1;
    }

    public get edition(): EditionData
    {
        return this.editionArgs.edition;
    }

    public get element(): string
    {
        return this.contentData.element;
    }

    public get extension(): string
    {
        return path.extname(this.filename);
    }

    public get filename(): string
    {
        let filename = path.join(this.directory, this.source);
        return filename;
    }

    public get format(): Formatter
    {
        return this.editionArgs.format;
    }

    public get linear(): boolean | undefined
    {
        return this.contentData.linear;
    }

    public get theme(): Theme
    {
        return this.editionArgs.theme;
    }

    public get logger(): any
    {
        return this.editionArgs.logger;
    }
}
