## [3.1.5](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.4...v3.1.5) (2018-08-12)


### Bug Fixes

* moved dev deps out of deps ([64614f6](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/64614f6))

## [3.1.4](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.3...v3.1.4) (2018-08-09)


### Bug Fixes

* fixing tsconfig.json to put files in right place ([f16365b](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/f16365b))

## [3.1.3](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.2...v3.1.3) (2018-08-07)


### Bug Fixes

* fixing main inside package.json ([e82405a](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/e82405a))

## [3.1.2](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.1...v3.1.2) (2018-08-07)


### Bug Fixes

* adding NPM publish to semantic release ([7d3901a](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/7d3901a))

## [3.1.1](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/compare/v3.1.0...v3.1.1) (2018-08-07)


### Bug Fixes

* organizing code base ([70ac808](https://gitlab.com/mfgames-writing/mfgames-writing-contracts-js/commit/70ac808))
